import React, { Component } from 'react';
import {createStore} from 'redux';
const store = createStore(reducer);

import {Provider} from "react-redux";
import reducer from "./store/reducer";
import Calculator from "./Calculator/Calculator";


class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Calculator />
            </Provider>
        );
    }
}

export default App;

