import actionType from './action';

const initialState = {

    value: '',
    isTurn: false

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.ADD_ELEM:

            return {...state, value: state.value + action.text};
        case actionType.CLEAR:
            return {...state, value: state.value.slice(0, -1)};
        case actionType.CLEAR_ALL:
            return {...state, value: ''};
        case actionType.RESULT:
            try {
                    const result = eval(state.value);
                    return {...state, value: result};
                } catch (e) {
                return {...state, value: 'error'}
            }
        default:
            return state;
    }
};

export default reducer;