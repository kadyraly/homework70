import React, {Component} from  'react';

import {connect} from 'react-redux';
import {
    StyleSheet,
    View,
    Button,
    Text, TouchableOpacity
} from 'react-native';

class Calculator extends Component {

    btns = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
    render() {
        return (
            <View style = {styles.PinCode}>
                <Text
                    style={{height: 60, width: 400, backgroundColor: '#fff', borderColor: 'gray', borderWidth: 1, fontSize: 40}}

                >{this.props.value}</Text>
                <View style={styles.Btn}>
                    {this.btns.map((btn) => {
                        return  <TouchableOpacity key={btn} style={styles.button}
                                                  onPress={() => this.props.addElem(btn)}><Text style={{fontSize: 40}}>{btn}</Text></TouchableOpacity>
                    })}

                    <TouchableOpacity style={styles.button} onPress={this.props.clear}><Text style={{fontSize: 40}}>{'c'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addElem('+')}><Text style={{fontSize: 40}}>{'+'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addElem('-')}><Text style={{fontSize: 40}}>{'-'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addElem('*')}><Text style={{fontSize: 40}}>{'*'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addElem('/')}><Text style={{fontSize: 40}}>{'/'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.props.result}><Text style={{fontSize: 40}}>{'='}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.props.clearAll}><Text style={{fontSize: 40}}>{'CE'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button}><Text style={{fontSize: 40}}>{'+M'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button}><Text style={{fontSize: 40}}>{'-M'}</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button}><Text style={{fontSize: 40}}>{'%'}</Text></TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return{
        value: state.value,
        isTurn: state.isTurn
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        addElem: (text) => {
            dispatch({
                type: 'ADD_ELEM',
                text: text
            })
        },
        clear: (text) => {
            dispatch({
                type: 'CLEAR',
                text: text
            })
        },
        result: (text) => {
            dispatch({
                type: 'RESULT',
                text: text
            })
        },
        clearAll: (text) => {
            dispatch({
                type: 'CLEAR_ALL',
                text: text
            })
        },
    }
};
export default connect(mapStateToProps, mapDispatchToProps )(Calculator);

const styles = StyleSheet.create({
    PinCode:{
        backgroundColor: "blue",
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,

    },
    Input: {
        backgroundColor: 'white',
        height: 80,
        width: '90%'
    },
    Btn: {
        overflow: 'hidden',
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        flexWrap: 'wrap'

    },
    button: {
        width: '20%',
        backgroundColor: "#841584",
        margin: 10,
        alignItems: 'center'
    }

});